package modelo;

import controlador.datosMatriz;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Matriz {

    private int acumulado = 0;
    private int tamanoMatriz = 0;
    private int resul = 1;
    Scanner teclado = new Scanner(System.in);
    Constraints v = new Constraints();

    /**
     * ValorMatriz()
     * @return int [][][] 
     * Actualiza Valores de una Matriz
     */
    public int[][][] ValorMatriz() {
        int W[][][];

        //intanciamos los Objetos de la Matriz
        datosMatriz dm = new datosMatriz();

        //validamos datos de la Matriz
        do {
            try {
                System.out.print("Fila de la matriz:");
                dm.fila = teclado.nextInt();
                System.out.print("Columnas de la matriz:");
                dm.columna = teclado.nextInt();
                System.out.print("Profundidad de la matriz:");
                dm.profundidad = teclado.nextInt();

                //Actualizamos el Valor de W
                W = new int[dm.fila][dm.columna][dm.profundidad];
                //Determinamos el Tamañp de la Matriz
                tamanoMatriz = dm.fila * dm.columna * dm.profundidad;
                break;

                //Si el dato es incorrecto ocurre una exception
            } catch (InputMismatchException e) {
                System.err.println("Error al introducir Datos");
                System.err.println("Por Favor Intente de Nuevo");
                teclado.nextLine();
                continue;
            }

        } while (true);

        return W;

    }

    /**
     * @param valoresM
     * @param ram
     * update return metodo de construcción de una Matriz Cubica
     */
    public void updateMatriz(int valoresM[][][], Random ram) {

        for (int f = 0; f < valoresM.length; f++) {
            for (int c = 0; c < valoresM[f].length; c++) {
                for (int p = 0; p < valoresM[f][c].length; p++) {
                     //pregunta si el random no es null
                     //Si es null se teclea los componentes de la matriz
                    if (ram == null) {
                        System.out.print("Ingrese el componente(" + resul++ + ") de (" + tamanoMatriz + "): ");
                        valoresM[f][c][p] = teclado.nextInt();
                    } else {
                        System.out.print("Ingrese el componente(" + resul++ + ") de (" + tamanoMatriz + "): ");
                        valoresM[f][c][p] = ram.nextInt(100);
                    }
                    //se calcula el total de Acumulado
                    acumulado = acumulado + valoresM[f][c][p];
                }

            }
        }
    }

    /**
     * queryAcum
     * @param valoresM
     * Dibuja la Matriz
     */
    public void queryAcum(int[][][] valoresM) {
        for (int f = 0; f < valoresM.length; f++) {
            for (int c = 0; c < valoresM[f].length; c++) {
                for (int p = 0; p < valoresM[f][c].length; p++) {
                    System.out.print(valoresM[f][c][p] + " , ");
                }
            }
            System.out.println();
        }
        System.out.println("Total Acumulado: " + acumulado);
    }
    
    /**
     * abjuntarMetodo()
     * Abjunta los metos en secuencia
     * Asi el usuario solo tiene que llamar a esta instancia para su ejecución 
     * 
     */
    public void abjuntarMetodo() {

        int[][][] vm = ValorMatriz();

        if (!v.validatorMatriz(tamanoMatriz)) {
            if (!v.vadatorCelda(vm)) {
                updateMatriz(vm, null);
                queryAcum(vm);
            } else {
                System.err.print("Excede el Tamaño de la Celda\n");
            }

        } else {
            System.err.print("Excede el Tamaño de la Matriz\n");
        }

    }

    /**
     * testUpdateMatriz
     * @param vm 
     * Metodo para las pruebas unitarias de la clase
     */
    public void testUpdateMatriz(int[][][] vm) {
       
        Random ab = new Random();
        updateMatriz(vm, ab);
        queryAcum(vm);

    }
}
