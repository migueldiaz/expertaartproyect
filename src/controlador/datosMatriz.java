/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

public class datosMatriz {
    
  public  int fila;
  public  int columna;
  public  int profundidad;

       public datosMatriz() {
    }
       
    public datosMatriz(int fila, int columna, int profundidad) {
        this.fila = fila;
        this.columna = columna;
        this.profundidad = profundidad;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getProfundidad() {
        return profundidad;
    }

    public void setProfundidad(int profundidad) {
        this.profundidad = profundidad;
    }

 
    
    
    
}
